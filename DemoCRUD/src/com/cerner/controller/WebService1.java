package com.cerner.controller;


import com.cerner.service.StudentService;

import javax.ws.rs.GET;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@Path("/Read")

public class WebService1 {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/all")
    public Response GetAllRecords()
    {
		System.out.println("Here");
		String result = StudentService.GetAllRecords(); 
		if(result == null)
		{
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.ok().entity(result).build();
	}

	
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{StudentId}")
    public Response GetRecordById(@PathParam("StudentId") String StudentId)
    {
    	
    	if(StudentId == null)
	   {
		return Response.status(Status.BAD_REQUEST).build();
	   }
    	
	String result = StudentService.GetRecordById(StudentId); 
	
	if(result == null)
	  {
		return Response.status(Status.NOT_FOUND).build();
	  }
	return Response.ok().entity(result).build();
   
    }
    

    
    
    
}
    
	
     



