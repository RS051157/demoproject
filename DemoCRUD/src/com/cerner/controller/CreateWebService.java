package com.cerner.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.cerner.service.StudentService;

@Path("/create")
public class CreateWebService {
	 @POST
	    @Consumes(MediaType.APPLICATION_JSON)
	    public Response CreateStudent(String jsonString)
	    {
	    	StudentService.CreateStudent(jsonString);
			return Response.ok().entity(jsonString).build();
	    }
	
	
	  }
	

