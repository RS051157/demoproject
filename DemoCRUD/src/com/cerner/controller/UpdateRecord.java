package com.cerner.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.cerner.service.StudentService;

@Path("/update")
public class UpdateRecord {

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response UpdateStudent(String jsonString)
    {
    	StudentService.UpdateStudent(jsonString);
		return Response.ok().entity(jsonString).build();
    }
}
