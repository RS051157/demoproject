package com.cerner.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


import com.cerner.service.StudentService;


@Path("/delete")
public class DeleteWebService {

	  @GET
	    @Produces(MediaType.APPLICATION_JSON)
	    @Path("/{StudentId}")
public static void DeleteRecordById(@PathParam("StudentId") String StudentId)
{
	
	StudentService.DeleteRecordById(StudentId);
}
}
