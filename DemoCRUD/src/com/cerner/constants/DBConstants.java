package com.cerner.constants;

public interface DBConstants {

	public static final String DB_CLASS="com.mysql.jdbc.Driver";
	public static final String DB_URL="jdbc:mysql://localhost:3306/cruddatabase";
	public static final String USER = "root";
	public static final String PWD = "admin";

}
