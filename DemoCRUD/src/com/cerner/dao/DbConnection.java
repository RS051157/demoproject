package com.cerner.dao;

import java.sql.Connection;
import java.sql.DriverManager;

import static com.cerner.constants.DBConstants.DB_CLASS;
import static com.cerner.constants.DBConstants.DB_URL;
import static com.cerner.constants.DBConstants.USER;
import static com.cerner.constants.DBConstants.PWD;

public class DbConnection {
	Connection con;
	public DbConnection()
	{
	    try 
	    {
				    Class.forName(DB_CLASS);
		    System.out.println("Driver loaded sucessfully");
		
	    }
	    catch(Exception e)
	    {
				System.out.println("Unable to load the driver");
	    }
	}
	
	public Connection setConnection()
	{
		
		try
		{			
			con = DriverManager.getConnection(DB_URL,USER,PWD);
			System.out.println("Database connection successful");
			return con;			
		}
		catch(Exception e)
		{
			
			System.out.println("Error connecting to the database.");
			e.printStackTrace();
			return null;
		}
		
	}
}
