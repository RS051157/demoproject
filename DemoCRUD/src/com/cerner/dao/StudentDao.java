package com.cerner.dao;

import com.cerner.dao.DbConnection;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;



public class StudentDao {
	DbConnection dbCon=new DbConnection();
	Connection con=null;
	PreparedStatement preparedStatement=null;
	ResultSet resultSet=null;
	
	public ResultSet ReadAll()
	{
		try {
			String sql="Select * from studentinfo";			
			
			con = dbCon.setConnection();
			preparedStatement = (PreparedStatement) con.prepareStatement(sql);
			
			resultSet = preparedStatement.executeQuery();
		}
		catch(Exception e)
		{
			System.out.println("Error in Reading");
			e.printStackTrace();
		}
		return resultSet;
	}
	
	
	public ResultSet GetRecordById(int StudentId)
	{
		try{
			String sql="Select * from studentinfo where Sid= ?";			
			
			con = dbCon.setConnection();
			preparedStatement = (PreparedStatement) con.prepareStatement(sql);
			preparedStatement.setInt(1,StudentId);
			resultSet = preparedStatement.executeQuery();
	           
			//System.out.print(resultSet.getString("StudentName"));
		}
		catch(Exception e)
		{
			System.out.println("Error in GetrecordById()");
			e.printStackTrace();
		}
		return resultSet;
	}

	
	
	public void UpdateStudent(int Sid,String StudentName,int Age)
	{
		try{			
			con = dbCon.setConnection();
			
			String query = "update studentinfo set StudentName=?,Age=? where Sid = ?";
		    preparedStatement = (PreparedStatement) con.prepareStatement(query);
		    preparedStatement.setString(1,StudentName);
		    preparedStatement.setInt(2, Age);
		    preparedStatement.setInt(3, Sid);	
		   
		    preparedStatement.executeUpdate();
			
			//query = "update studentinfo set status=\"booked\", PersonId= ? where ApptID = ?;";
			//preparedStatement = (PreparedStatement) con.prepareStatement(query);
			//preparedStatement.setInt(1, personId);
			//preparedStatement.setInt(2, apptId);
			//preparedStatement.executeUpdate();
		}
		catch(Exception e)
		{
			System.out.println("Error in updateStudents");
			e.printStackTrace();
		}		
	}
	public void CreateStudent(int Sid,String StudentName,int Age)
	{
		
		try{
			con = dbCon.setConnection();
			
			String query = "Insert into studentinfo values(?,?,?)";
			preparedStatement = (PreparedStatement) con.prepareStatement(query);
		    preparedStatement.setInt(1,Sid);
		    preparedStatement.setString(2, StudentName);	
		    preparedStatement.setInt(3, Age);
		    preparedStatement.executeUpdate();
			
		}
		catch(Exception e)
		{
			System.out.println("Error in CreateStudents");
			e.printStackTrace();
		}		
	}
	
	public void DeleteRecordById(int StudentId)
	{
		try{
			String sql="Delete from studentinfo where Sid= ?";			
			
			con = dbCon.setConnection();
			preparedStatement = (PreparedStatement) con.prepareStatement(sql);
			preparedStatement.setInt(1,StudentId);
			 preparedStatement.executeUpdate();
	           
			//System.out.print(resultSet.getString("StudentName"));
		}
		catch(Exception e)
		{
			System.out.println("Error in DeleteRecordById()");
			e.printStackTrace();
		}
	}



}

