package com.cerner.service;

import java.sql.ResultSet;
import java.util.List;

import org.json.JSONObject;

import com.cerner.dao.StudentDao;
import com.cerner.utils.ResultSetToJSON;
import com.cerner.utils.JSONParse;
import org.json.JSONException;
public class StudentService{
	
	static StudentDao obStudentDao = new StudentDao();
	
	 public static String GetAllRecords() 
	 {
		 
		 ResultSet resultSet=null;
         
         resultSet = obStudentDao.ReadAll();
         List<JSONObject> resList = ResultSetToJSON.convertJSON(resultSet);    		
         
         return resList.toString();
	 }
	 
	 public static String GetRecordById(String StudentId) 
	 {
		 
		  ResultSet resultSet=null;
          
          resultSet = obStudentDao.GetRecordById(Integer.parseInt(StudentId));
 			List<JSONObject> resList = ResultSetToJSON.convertJSON(resultSet);
 			return resList.toString();
		
         
	 }
	 
	 public static void CreateStudent(String jsonString)
	 {
		 
		 try {
			 
				JSONParse.parseJSONToCreate(jsonString);
	           }
	           catch (JSONException e) {
				e.printStackTrace();
				System.out.println("Error while calling parseJSONToCreate()");
			   }
	 }
	 
	 public static void UpdateStudent(String jsonString)
	 {
		 try {
			 
				JSONParse.parseJSONToUpdate(jsonString);
	           }
	           catch (JSONException e) {
				e.printStackTrace();
				System.out.println("Error while calling parseJSONToUpdate()");
			   }
	 }
	 
	 public static void DeleteRecordById(String StudentId)
	 {  
		 
		 obStudentDao.DeleteRecordById(Integer.parseInt(StudentId));
		
	 }
}
