package com.cerner.utils;



import org.json.JSONException;
import org.json.JSONObject;

import com.cerner.dao.StudentDao;
public class JSONParse {
	public static void parseJSONToUpdate(String jsonString) throws JSONException
	{
		int Sid; String StudentName; int Age;
		JSONObject obj = new JSONObject(jsonString);
		Sid = (Integer.parseInt(obj.getString("Sid")));
		StudentName=obj.getString("StudentName");
		Age = (Integer.parseInt(obj.getString("Age")));
		StudentDao oStudentDao = new StudentDao();
		oStudentDao.UpdateStudent(Sid,StudentName,Age);
	}
	public static void parseJSONToCreate(String jsonString) throws JSONException
	{
		int Sid; String StudentName; int Age;
		JSONObject obj = new JSONObject(jsonString);
		Sid = (Integer.parseInt(obj.getString("Sid")));
		StudentName=obj.getString("StudentName");
		Age = (Integer.parseInt(obj.getString("Age")));
		StudentDao oStudentDao = new StudentDao();
		oStudentDao.CreateStudent(Sid, StudentName, Age);
		
		
	}
	
	
}
