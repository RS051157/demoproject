package com.cerner.utils;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

public class ResultSetToJSON {
		
		public static List<JSONObject> convertJSON(ResultSet rs)
		{
			
			List<JSONObject> resList = new ArrayList<JSONObject>();
			try 
			{
				
				ResultSetMetaData rsmd = rs.getMetaData();
				int columnCount = rsmd.getColumnCount();
				List<String> columnNames = new ArrayList<String>();
				for(int i=0; i<columnCount; ++i)
				{				
					columnNames.add(rsmd.getColumnName(i+1));				
				}
				while(rs.next())
				{
					JSONObject obj = new JSONObject();
					for(int i=0; i<columnCount; ++i)
					{
						String key = columnNames.get(i);
						String value = rs.getString(i+1);
						
						obj.put(key,value);
					}
					resList.add(obj);
				}
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				try
				{
					rs.close();
				}
				catch(SQLException e)
				{
					e.printStackTrace();
				}
			}
			
			return resList;
		}
}
